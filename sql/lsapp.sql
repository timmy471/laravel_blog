-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 17, 2019 at 03:24 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lsapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(5, '2014_10_12_000000_create_users_table', 1),
(6, '2014_10_12_100000_create_password_resets_table', 1),
(7, '2019_08_19_000000_create_failed_jobs_table', 1),
(8, '2019_11_14_183849_create_posts_table', 1),
(12, '2019_11_16_183617_add_user_id_to_posts', 2),
(14, '2019_11_16_211549_add_user_id_to_posts', 3),
(15, '2019_11_16_230216_add_cover_image_to_posts', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `cover_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `body`, `created_at`, `updated_at`, `user_id`, `cover_image`) VALUES
(16, 'Will.I.Am calls out Qantas flight attendant for being \'racist', 'Black Eyed Peas star Will.I.Am has called a Qantas flight attendant for allegedly being a \'racist\' following an experience on board a flight from Brisbane to Sydney today November 16. \r\n\r\n \r\n\r\nThe singer who is performing with his bandmates in Auckland on Sunday night for Friday Jams along with pop superstar Janet Jackson, 50 Cent and Jason Derulo, said the \'overly-aggressive\' flight attendant tried getting him arrested because he couldn\'t hear her through his noise-cancelling headphones.\r\n\r\n \r\n\r\n\"I\'m sorry to say me and my group have experienced they (sic) worse service due to a overly aggressive flight attendant... I don\'t want to believe she racist. But she has clearly aimed all her frustrations only at the people of colour,\" he tweeted.', '2019-11-16 23:56:49', '2019-11-16 23:56:49', 3, 'picture1_1573952209.jpeg'),
(18, 'Again, several solutions like eye drops, pills and surgery was recommended.', 'You see, I was once in your shoes, I used to battle severe eye problems that made me had blurred vision, dry itchy eyes.\r\n\r\nBut my eye doctor told me I could only live with it, so my optician kept on recommending eye glasses lens (or spectacles) so I could manage it to improve my vision.\r\n\r\nBut, as time went by... this became ineffective, caused several eye irritations and also negatively affected my self esteem.\r\n\r\n3 years later, my eye condition had worsened... after several eye exams, I was diagnosed with glaucoma in the right eye.\r\n\r\nAgain, several solutions like eye drops, pills and surgery was recommended.\r\n\r\nEye surgery which was the best long term treatment was really expensive, and even comes with its own side effects after several months or years.\r\n\r\nSo, I decided to start using eye drops just so I could manage to see because at this point, I was literally going blind.\r\n\r\nYou see, the problem with eye drops is that most of them came with serious side effects, so one has to continue changing them until you see the most suitable one', '2019-11-17 00:08:51', '2019-11-17 00:08:51', 3, 'picture2jpeg_1573952931.jpeg'),
(19, '\'\'Delay is not denial\'\' Nigerian lady who failed JAMB on her first attempt, rejoices as she bags first class and graduates top in her department', 'Toun Banjo, a Nigerian lady has taken to her Twitter handle to express her joy at becoming a first class graduate and best graduating student of her department despite failing the Joint Admis .\r\na Nigerian lady has taken to her Twitter handle to express her joy at becoming a first class graduate and best graduating student of her department despite failing the Joint Admis .', '2019-11-17 00:11:07', '2019-11-17 00:11:07', 1, 'picture3_1573953067.jpeg'),
(21, 'Paul Pogba and his partner, Maria Zulay Salaues have revealed their son\'s face for the very first time.', 'Paul Pogba and his partner, Maria Zulay Salaues have revealed their son\'s face for the very first time. \r\n\r\n \r\n\r\nThe Manchester United midfielder took Instagram to celebrate the Bolivian model on her birthday, being today, November 16, 2019, and then shared beautiful family photos. \r\n\r\n \r\n\r\nIn one of the images, Pogba and his partner who recently changed her name to Zulay Pogba on Instagram, sparking marriage rumors were pictured posing with their adorable son while they were all clad in matching Versace ensembles. \r\n\r\n\r\nThe pair welcomed their child in January 2019.', '2019-11-17 00:15:48', '2019-11-17 00:15:48', 1, 'picture3_1573953348.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Timmy', 'ayantunjitimilehin@gmail.com', NULL, '$2y$10$oICKbaOwatxBlsuoD8m/t.ZmoLAbI.gfXqGMkSBFEejjfa3RTIFYS', 'zmJKeFuVZyp6PJAqOSKautXeESvbkJbnEE3ilZzfkSGOVVZk8G49k8kTmuV0', '2019-11-16 16:52:21', '2019-11-16 16:52:21'),
(2, 'Imosi', 'imosi@yahoo.com', NULL, '$2y$10$I0TIMbbnANQsCLyAU/wRFeq1j4fvlYRMyR.apsgCylLUPsvY4rC5q', NULL, '2019-11-16 20:27:54', '2019-11-16 20:27:54'),
(3, 'Soji', 'soji@yahoo.com', NULL, '$2y$10$QaY5kiDq6KnM0NnLKHO.W.hvTW1QWcfs3qP24HEDSuDc3mvs.ysoG', NULL, '2019-11-16 20:35:54', '2019-11-16 20:35:54');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
