@extends('pages.layouts.master')

@section('content')
<div>
<a href = "/laravelProjects/lsapp/public/posts" class = 'btn btn-primary'>Go back!</a>
<h4>{{$post->title}}</h4> &nbsp;
<img height="60%" width="80%" src = "/laravelProjects/lsapp/public/storage/cover_images/{{$post->cover_image}}"><br><br>
<small>written by {{$post->user->name}} on {{ $post->created_at }} </small>
<hr>
<p>{{$post->body}}</p>
@if(!Auth::guest())
    @if(Auth::user()->id == $post->user_id)
    <a href = '/laravelProjects/lsapp/public/posts/{{ $post->id }}/edit' class = 'btn btn-secondary'>Edit</a>

    {!! Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST', 'class' => 'd-inline']) !!}
        {{ Form::hidden('_method', 'DELETE') }}
        {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
    {!! Form::close() !!}
    @endif
@endif
</div>



@endsection