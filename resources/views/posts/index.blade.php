@extends('pages.layouts.master')

@section('content')
<h1>Posts</h1>
    <div>
        @if(count($posts)>0)
        
        @foreach($posts as $post)
        <div class = 'wells'>
            <div class = 'row'>
                <div  class = 'col-md-d col-sm-4'>
                <img  height='50%' width='90%' src = "/laravelProjects/lsapp/public/storage/cover_images/{{$post->cover_image}}">
                </div>
                <div class = 'col-md-d col-sm-8'>
                     <h4><a href = "/laravelProjects/lsapp/public/posts/{{$post->id}}">{{ $post->title }}</a></h4>
                     <small><strong>written by {{$post->user->name}} on {{ $post->created_at }} </strong></small>
                </div>

            
            
           
            <hr>
        </div>
           
        @endforeach
        
        @else
        <p>no posts found</p>
        @endif
        {{$posts->links()}}
        

    </div>
@endsection