@extends('pages.layouts.master')

@section('content')
<h1>Create Post</h1>

{!! Form::open(['action' => 'PostsController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
    <div class = 'form-group'>
            {{Form::label('title', 'Title')}}
            {{Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'What is your post title?', 'autocomplete' =>'off'])}}
    </div>

    <div class = 'form-group'>
            {{Form::label('body', 'Body')}}
            {{Form::textarea('body', '', ['class' => 'form-control', 'placeholder' => 'Wat is your content?', 'autocomplete' =>'off'])}}
    </div>

    <div class = 'form-group'>
        {{Form::file('cover_image')}}
 </div>
    {{Form::submit('Click Me!', ['class' => 'btn btn-primary'])}}

    
{!! Form::close() !!}
    
       
       
@endsection