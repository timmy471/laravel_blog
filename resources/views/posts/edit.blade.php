@extends('pages.layouts.master')

@section('content')
<h1>Edit Post</h1>

{!! Form::open(['action' => ['PostsController@update', $post->id], 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
    <div class = 'form-group'>
            {{Form::label('title', 'Title')}}
            {{Form::text('title',  $post->title, ['class' => 'form-control', 'placeholder' => 'What is your post title?', 'autocomplete' =>'off'])}}
    </div>

    <div class = 'form-group'>
            {{Form::label('body', 'Body')}}
            {{Form::textarea('body',  $post->body, ['class' => 'form-control', 'placeholder' => 'What is your content?', 'autocomplete' =>'off'])}}
    </div>
     {{ Form::hidden('_method', 'PUT')}}  {{--  this is essential to specify method --}}
     {{-- <div class = 'form-group'>
                {{Form::file('cover_image')}}
         </div> --}}
    {{Form::submit('Click Me!', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}
    
       
       
@endsection