@extends('pages.layouts.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h3>Your Blog posts</h3>
                    @if(count($posts)>0)
                     <table class = 'table table-striped'>
                        <tr>
                            <th>Title</th>
                            <th>Action</th>
                            <th>Author</th>
                            
                        </tr>
                        
                        @foreach($posts as $post)
                            <tr>
                                <td>{{$post->title}}</td>
                                <td>
                                    <a href = '/laravelProjects/lsapp/public/posts/{{$post->id}}/edit' class = 'btn btn-primary'>Edit</a>
                                    {!! Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST', 'class' => 'd-inline']) !!}
                                    {{ Form::hidden('_method', 'DELETE') }}
                                    {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                                    {!! Form::close() !!}                               
                                </td>
                                <td>{{$post->user->name }}</td>
                                
                            </tr>
                        @endforeach
                    </table>
                    @else
                    <p> You have no posts created yet</p> 
                    @endif
                    <a href = '/laravelProjects/lsapp/public/posts/create' class = 'btn btn-primary'>Create Post</a></
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
