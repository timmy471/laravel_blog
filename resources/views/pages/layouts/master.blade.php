<!DOCTYPE html>
<head>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel = 'stylesheet' href ="{{asset('css/app.css')}}">
        <title>{{ config('app.name') }}</title>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" 
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">        
        
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    
    </head>

    <body>
            @include('pages.inc.navbar') 
          
            <div class = 'container'>
             @include('pages.inc.messages')
           
             @yield('content')

        </div>
    </body>
</html>
 