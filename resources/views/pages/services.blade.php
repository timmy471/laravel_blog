 @extends('pages.layouts.master')

@section('content')
<h1> {{ $title }}</h1>

<ul class = 'list-group'>
@foreach($content as $content)
<li class = 'list-group-item'>{{ $content }}</li>
@endforeach
</ul>
@endsection
   