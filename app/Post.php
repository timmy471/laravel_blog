<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Post;

class Post extends Model
{
    protected $table = 'posts';
    public $primarykey = 'id';
    public $timestamps = true;

    public function user(){
        return $this->belongsTo('App\User'); //a single post belongs to a user
    }
}
