<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use App\Post;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    //except logged in, disallow showing all other pages except index and show views
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }


    public function index(){
        // $posts =  post::all();
        // $posts =  post::where('title', 'Post Two');
        $posts = post::orderBy('created_at', 'desc')->paginate(3);
        //$posts = post::orderBy('created_at', 'desc')->get();  post is a method on our model which weve called in by use App\post, using eloquent ORM
        return view('posts.index')->with('posts', $posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'cover_image' => 'image|nullable|max:199'
            ]);
            //handle file upload
            if($request->hasFile('cover_image')){
                //Get filename with the extension
                $fileNameWihExt = $request->file('cover_image')->getClientOriginalName();
                // Get filename
                $filename =  pathinfo($fileNameWihExt, PATHINFO_FILENAME);
                //get just ext
                $extension = $request->file('cover_image')->getClientOriginalExtension();
                //file name to store
                $fileNameToStore = $filename.'_'.time().'.'.$extension;
                //upload image
                $path = $request->file('cover_image')->storeAs('public/cover_images', $fileNameToStore);
            }else{
                $fileNameToStore = 'noImage.jpg';
            }
        

        //to create the new post from user input
        $post = new Post;  //create a new post in the model (like we did with tinker) which is connected to our db..remember model migrate thing like we did with tinker 
        $post->title =  $request->input('title'); //this is equivalent to saying REQUEST['title] in php
        $post->body =  $request->input('body');
        $post->user_id = auth()->user()->id;
        $post->cover_image = $fileNameToStore;
        $post->save();
        

        return redirect ('/posts')->with('success', 'Post created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $post = post::find($id);
       return view('posts.show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $post = post::find($id);
         //to be sure its the owner thats trying to edit the post
        if(auth()->user()->id !==$post->user_id){
            return redirect('posts')->with('error', 'You are not allowed to edit this post as your are not the creator');

        }

       return view('posts.edit')->with('post', $post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'cover_image' => 'image|nullable|max:199'
            ]);
            //handle file upload
            if($request->hasFile('cover_image')){
                //Get filename with the extension
                $fileNameWihExt = $request->file('cover_image')->getClientOriginalName();
                // Get filename
                $filename =  pathinfo($fileNameWihExt, PATHINFO_FILENAME);
                //get just ext
                $extension = $request->file('cover_image')->getClientOriginalExtension();
                //file name to store
                $fileNameToStore = $filename.'_'.time().'.'.$extension;
                //upload image
                $path = $request->file('cover_image')->storeAs('public/cover_images', $fileNameToStore);
            }

        //to create the new post from user input
        $post = Post::find($id);  //we are just going to find the id of the post submited from edit
        $post->title =  $request->input('title'); //this is equivalent to saying REQUEST['title] in php
        $post->body =  $request->input('body');
        if($request->hasFile('cover_image')){
            $post->cover_image = $fileNameToStore;
        }
        $post->save();

        return redirect ('/posts')->with('success', 'Post updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $post = Post::find($id);
       //to be sure its the owner thats trying to edit the post
       if(auth()->user()->id !==$post->user_id){
        return redirect('posts')->with('error', 'You are not allowed to edit this post as your are not the creator');
        if($post->cover_image != 'noimage.jpg'){
            //Delete image
            Storage::delete('\laravelProjects\lsapp\public\cover_images'.$post->cover_image);

        }
    }
       $post->delete();
       return redirect ('/posts')->with('success', 'Post deleted successfully');
    }
}
