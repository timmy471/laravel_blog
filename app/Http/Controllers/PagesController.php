<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function services(){
        $data = [
            'title' => 'Services',
            'content' => ['web design', 'app developmet', 'SEO', 'Mobile Apps']
        ];
        return view('pages.services')->with($data);
    }

    public function about(){
        return view('pages.about');
    }

    public function index(){
        return view('pages.index');
    }
}
