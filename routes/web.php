<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'uses' => 'PagesController@index',
    'as' => 'pages.index'
]);

// Route::get('/about', [
//     'uses' => 'PagesController@about',
//     'as' => 'pages.about'
// ]);

Route::get('/about',  'PagesController@about');

Route::get('/services', [
    'uses' => 'PagesController@services',
    'as' => 'pages.services'
]);

Route::resource('posts', 'PostsController');
Auth::routes();

Route::get('/dashboard', 'dashboardController@index')->name('dashboard');

Auth::routes();

Route::get('/dashboard', 'dashboardController@index')->name('dashboard');



Auth::routes();

Route::get('/dashboard', 'DashboardController@index');
